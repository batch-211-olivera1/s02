Introduction to Git

VCS/ Version Control System
	VCS is a piece of software that manages/controls the revisions of a source code/document/collection of information. This allows the programmer/developer to create versions.

What is Git
	Git is an open source VCS that we use to track changes in a file within a repository.

Repositories
	Local Repositories - initialized folders that use git technology. Therefore, it allows us to track changes made in the files with the folder. These changes can then be uploaded and updated in the remote repositories.

	Remote Repositories - are folders that use git technology but instead of being in local machine, they are located in the internet/cloud such as GiLab and GitHub.

SSH Key
	Secure Shell Key - used to authenticate the uploading/pushing or doing other tasks when manipulating or using git repos. Passwords will not be required once SSH keys are set.

DETAILED STEPS IN PUSHING

1. create the folder that you wish to upload to Gitlab
2. add the necessary files in the folder
3. use git init command to initialize the folder into local repository. "(master)" must be seen in the gitbashterminal i.e. "~/Desktop/batch-211/s02 (master)"
	-NOTE!!!: MAKE SURE THAT YOU ARE IN THE SESSION FOLDER (PARENT FOLDER) BEFORE USING THIS COMMAND, OTHERWISE THERE WILL BE LOTS OF PROBLEMS IN PUSHING THE FILES
4. use git add to stage the files in the folder for commit

5. use git commit -m "<message>" to create a version of the folder

6. use git status to check if there are any files to be comitted (if there are none, "nothing to commit, working tree clean" should appear in the gitbash/terminal)

7. go to your gitlab account and go to your batch group.
	-if you do not have batch group yet, create one and make sure it is PUBLIC, otherwise, your activities cannot be checked

8. inside the batch repository, create a new blank project.
	-create a name for the project, use the prescribed name for convinience i.e. s02, s03, s04
	-make sure that the project is set to public, otherwise your activity cannot be checked
	-untick/uncheck all the checkboxes under the "Project Configuration" since you are to upload your local repository
	-create the project

9. in the project interface, click the "Clone" button and copy the code under "Clone with SSH"

10. go back to gitbash and type "git remote add origin <CloneWithSSHCode>" and replace the <CloneWithSSHCode> with the code you just copied from the gitlab
	-"origin" is the default alias for remote repositories in gitbash, however you can set your own alias for this. Just make sure you will remember the alias since it is important for pushing the files

11. use git remote -v to check if you have linked your remote repository to your local repository

12. use git push origin master to upload your local repository to your gitlab repository

13. go back to gitlab to check if you have uploaded your local repository to the project you created a while ago. Congrats!


Basic Commands

	git init - initialize a new local git repository from common directories.

		REMINDER: THERE SHOUDL BE NO .git folder INSIDE THE BATCH FOLDER

		OR

		DISCUSSION/ACTIVITY FOLDERS;
		
		IT IS ONLY FOUND INSIDE THE SESSION FOLDER i.e. s02

	DELETE the .git folder if you have initialized the wrong folder
	

	ssh-keygen - allows the devs to create ssh key for the device to be used in pushing to GitLab/GitHub


	
	git remote add <alias> <gitSSHURL> - allows to connect our local repo to an online/remote repos. "origin" is the default alias for remote repositories

	ex. git remote add origin <url>

	git remote remove <alias> - allows to delete an existing online/remote repo that is connected to the local repo

	ex. git remote remove origin

	git add (git add .) - allows staging of the files to track created files/modified files before creating commits in version creation.
		git add . - we will stage all files that are needed to be committed whether they are untracked, modified, or deleted

	ex. git add .

	git commit - used to create a version of the files/repo that have already been staged (added/ included in the git add command).
		flag
			-m - stands for message; indicates that the next text enclosed in quotaion marks is the commit message for the version.

		commit message - a detailed description of what is changed/included in the version. 
			REMINDER: the commit message is always starting with a verb and is always concise as a norm for all devs

			ex. git commit -m ""

	git log - tracks the versions of the repo/project.
		adding --oneline - returns the summary of the versions of the project


	git remote -v - allow us to check the remote connections of our local repo.

	git status - allows to check/peak for files that are not yet added or committed.



		mkdir - allows creation of directories

		rmdir - allows removing of directories (Do not use commands if you are not sure, these can lead to more chances of errors)

		touch - allows creation of files

		cd - allows changing of directories
